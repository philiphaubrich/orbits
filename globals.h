#ifndef __GLOBALS__
#define __GLOBALS__

#include <time.h>

#define false 0
#define true 1

#define BUFFSIZE 1000
#define ARRAYSIZE 512
#define PI 3.14
#define RADIAN_TO_DEGREE 57.2957795

#define NUM_PLANETS 9
#define MAX_NUM_SHIPS 100     
#define DEFAULT_NUM_SHIPS 50

#define ROUND_LENGTH_DEFAULT 15000 //can't be < 256 without other parts breaking.
#define NUM_ROUNDS_DEFAULT 10

#define SHIPMASS 10
#define SHIPFUEL ROUND_LENGTH_DEFAULT*20 //20000 ticks, thrusting up to 20 makes an upper limit of 400,000
#define SHIPNAMESIZE 20

#define uint32 unsigned int
//#define uint unsigned int
#define uchar unsigned char

#define SCREEN_WIDTH 512
#define SCREEN_HEIGHT 512

//#define LOGGING_DEFAULT LOG_ERR
//#define LOGGING_DEFAULT LOG_ERR | LOG_HIGHGAME | LOG_MIDGAME | LOG_FITNESS
#define LOGGING_DEFAULT LOG_ALWAYS

#define PATHPRECISION 100   //How may slices those paths record

#define DNA_SIZE 256 
#define MUTATION_TYPES 11

//Hmmm, if you slice time too thin the initial pool is all pretty similar as their thrusts average out, but the longer time slice, the harder he GA to tweak...
#define DEFAULT_TIME_SECTION ((ROUND_LENGTH_DEFAULT/DNA_SIZE)*2)

#define GOOD_ENOUGH SCREEN_WIDTH*NUM_PLANETS*0.95 //+ SHIPFUEL * 0.1     


//oof, TODO: Try to ease off using so many globals. 
extern float g_maxFit;
extern int g_roundsSinceImprovement;
extern struct ships* g_kingoftheHill;
extern struct ships* g_newborn;

extern char g_buff[BUFFSIZE];
extern int g_curserPos;

extern char g_bugBuff[100];
extern float g_debug;




extern int g_numShips;
extern int g_shipSeed;
extern int g_planetSeed;
extern int g_time;
extern int g_round;	
extern int g_paused;
extern int g_topFit;
extern int g_next;
extern int g_nextImprovement;
extern int g_viewFitArray;
extern int g_previousTimeMark;

extern int g_playerActionIndex;
extern int g_displaySelection;

extern const clock_t QUARTERSEC;
extern const clock_t MILLISEC;
extern const clock_t NO_DISPLAY;
extern const clock_t MIN_DISPLAY;
extern const clock_t FASTEST;

extern clock_t g_speed;
extern int g_numRounds;
extern int g_roundLength;
extern int g_logging;

#endif
