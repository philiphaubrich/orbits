all: main.c orbit.o gplib.o display.o
	gcc -o orbit.exe main.c orbit.o gplib.o display.o -lmingw32 -lSDLmain -lSDL -lSDL_ttf  -g -Wall

display.o : display.c display.h globals.h
	gcc -c display.c `sdl2-config --cflags` -g -Wall  
  
orbit.o : orbit.c orbit.h gplib.o globals.h
	gcc -c orbit.c -g -Wall

gplib.o : gplib.c gplib.h orbit.h globals.h
	gcc -c gplib.c -g -Wall
	
	
clean : 
	rm -f *.o
	rm -f orbit.exe
	

  
