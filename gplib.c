/*
gplib.c library for genetic programming

Open source, GPLv3
Signed in blood, -Philip Haubrich, June 12th 2012

Remade for orbits
So, this is a simple application of GA, no genetic programming at all. The problem they face is static for their entire lineage, and they have no feedback on their surroundings until the end of the round and the selection phase. They are deaf-blind-and-dumb. This is still AI though, because given a problem they can form a solution. The ships are not intelligent, but the system of ships, GA, and fitness IS intelligent.   A different approach from the killbots.

Goal: Pass by as many planets as possible, as close as possible, spending the least amount of fuel, in a time period of X.

Hmmm, it's tempting to do something domain specific for helping ships.
Like, say, identifying when a ship crashes and what segments of dna were active at that time, and specifically tweaking that section.  Maybe even doing it every time a ship crashed, not bothering with selection. It would certainly help ships avoid crashes, but it's so domain specific, and this is all just a learning exercise into how to make GA work. 

I guess the larger idea here is to identify critical sections of the dna and play with those more than the non-critical sections. In gpFight, there are whole swaths of dna that aren't executed. Here there is dna that never gets reached because of a crash. So some generalized way to let the breeding/mutation GA know what sections are troublesome/fragile/needTweaking/need to be axed....

I also want to see what mutations types are helping or hurting. My mutation chart doesn't seem to agree between runs.  I think that whoever gets on top for runs gets their stats on top and that's just luck of the draw. It might not even be directly related as the crossbreeding function could have done all that jazz.

Perhaps we could try increasing the mutations when the top fitness is dormant. If he lingers there long enough, everyone turns into him and we're all the same.
AH, I got this one. When two breeders are identical, it means we're losing diversity. So give the offspring more mutations. IE, inbreeding makes for freaks.

As far as a demo goes, you need something more flashy. 
Mouse control would go a long way
Animating actions between rounds would demo just what the fuck is going on. You know, without me having to explain it by hand.
Actual ship images rather than an X.


Could do a separate log of noticeable events. Things at a higher level rather than the rote daily events like births and deaths. Analysing things to see interesting stuff. Because looking at this manually is really hard and time consuming.
-Increases to the maxFitness.  Which is the entire goal. 
-Is it and incremental improvement? when none of the minDistances of the winner changed by more than 10%? 
-Or is it a new path: When the minDist array looks significantly different than before
-Or an extension: When the minDistance array stays the same for some planets, but better for others. It'll probably mean he avoided collision and continued on.
-Entering and exiting state conditions:
-25%, 50%, 75% of the populace ending in the same location = stagnant.
-At least one ship has exhausted their DNA_SPACE
-Most ships have exhausted their DNA_SPACE
-Mutation success rate becomes flat.


Really need to experiment with what fucking works. Keep the planetary seed, and change the ship seed so you change things but still run the same course.
What's the right population size? (Remember, we're looking at real-time cost, not rounds)
What's the right mutation rate?
What's the right way to select agents?
What's the right crossbreeding method?  I mean, in this application mixing two flight paths is going to be crazy weird.
Does crossbreeding even work? Should we use asexual reproduction with just mutations?
What are the shit mutations and what actually advances the pool
Are mutations necessary?

You can guesstimate and have a scholarly answer and it won't mean shit without an experiment.


HEY! wait a minute!  There's ZERO reason to RE-calculate a ships's fitness. Once it goes through it's path, that's what it'll always be. We only ever need to crunch the newborns!  WHOA, my mind if freaking BLOWN. 

*/

#include "gplib.h"
#include "orbit.h"  
#include "display.h" 
#include "globals.h"

#include <unistd.h>
#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <math.h>

extern int betterRand(int n); 
extern int g_round;
extern int g_time;
extern int g_numShips;

float g_maxFit;
struct ships* g_kingoftheHill;
struct ships* g_newborn;
int g_roundsSinceImprovement=0;
int g_mutationRate = 3;



void updateFitness(struct ships pool[], struct planets targets[])
{
  int i,j;
  float dist;
  for(i=0; i< g_numShips; i++) 
  {
    if(pool[i].crashed)
      continue;
    for(j=0; j< NUM_PLANETS; j++)
    {
      dist = distance(pool[i].x, pool[i].y, targets[j].x, targets[j].y);
      if(pool[i].minDist[j] > dist)
        pool[i].minDist[j] = dist;
    }
  }
}



void chartLog()
{
  fprintf(f_fitchart,"%f",g_maxFit);
  if(g_kingoftheHill != NULL)
    fprintf(f_fitchart,", %f %d %d\n", g_kingoftheHill->finalFuel, g_kingoftheHill->finalCrashed, g_kingoftheHill->finalExhausted);  
  else
    fprintf(f_fitchart,"\n");  

  /* This has been more or less random and hasn't told me shit
     for(i=0; i< MUTATION_TYPES; i++) 
     {
     fprintf(f_mutationchart,"%d, ",g_mutationFitness[i]);
     }
     fprintf(f_mutationchart,"\n");

*/
}


//So, right now it's NumPlanets*ScreenWidth  9*512  
//    4608 max points they COULD get. And they try and shave off their losses. 
//Adding onto that. MaxFuel = ROUND_LENGTH_DEFAULT*20  15000*20  
//    300,000 max fuel points.   But let's trim that down to ~1000  so Divide by 300
//        But really more like (g_roundLength*20/1000) 
float shipFitness(struct ships* ship)
{
  int i;
  float fit = 0.0;

  //Rewarded for getting as close as possible to each planet
  for(i=0; i<NUM_PLANETS; i++)
  {
    fit += (float)SCREEN_WIDTH - ship->minDist[i];
  }

#ifdef USINGFUEL 
  //Rewarded for having leftover fuel,  only if you don't crash
  //Can't seem to balance this against distance...
  if(!(ship->crashed) && ship->fuel > 0)
    fit += ship->fuel/ (g_roundLength*20/1000);  
#endif

  return fit;
}

/*Ok so this
  1) sets the fitness value of ships according to their minDist array
  2) sets the g_king of the hill according to who is on top
  3) counts the number of rounds since the kind has changed.
//4) Outputs the recent record for the mutation chart. 
//5) Logs when a ship has exhausted it's DNA_SIZE

@param pool It's all the ships. 

... yeah, I should probably break this up to separate functions...
Sweet Jesus, did I name the function the same as the variable....!?   let's put a big 'ol TODO on that one!
*/
void finalFitness(struct ships pool[] )
{
  int i;
  float fit;
  float tmpMax;

  tmpMax = g_maxFit;

  for(i=0; i< g_numShips; i++) 
  {
    //That is, if we've calculated what their fitness value is, we don't have to go through this again.
    if(pool[i].finalFitness != 0)
      continue;

    //logging(LOG_FITNESS ,"%s: %f, %f\n",pool[i].name, pool[i].x, pool[i].y);
    //printDNA(&pool[i]);
    //if(pool[i].exhausted)
    //  logging(LOG_FITNESS ,"%s is exhausted\n",pool[i].name);

    fit = shipFitness(&pool[i]);
    pool[i].fitness = fit;
    pool[i].finalFitness = fit;
    pool[i].finalFuel = pool[i].fuel;
    pool[i].finalCrashed = pool[i].crashed;
    pool[i].finalExhausted = pool[i].exhausted;
    if(g_maxFit < fit)
    {
      g_maxFit = fit;
      g_kingoftheHill = &pool[i];
    }
  }
  logging(LOG_FITNESS ,"King of the hill: %s with %f.   %f to go\n",g_kingoftheHill->name, g_kingoftheHill->finalFitness,   g_kingoftheHill->finalFitness - GOOD_ENOUGH);

  if(g_maxFit > tmpMax)
  {
    g_roundsSinceImprovement = g_round;
  }
}

/*
   New selection method. We're doing simple tournament. 
   Out of the pool, pick 2 at random,  keep the better for breeding.
   Repeat so you have two to breed. 
   Repeat all that till you have enough kids.   ...like, one.
   */
void selection(struct ships pool[] )
{
  struct ships* combantant1;
  struct ships* combantant2;
  struct ships* dadCompetitor1;
  struct ships* dadCompetitor2;
  struct ships* momCompetitor1;
  struct ships* momCompetitor2;
  struct ships* dad;
  struct ships* mom;
  struct ships* toDie;
  int i;

  finalFitness(pool);

  for(i=0; i< 1; i++) //just one birth/death for now
  {
    combantant1 = &pool[betterRand(g_numShips-1)+1];
    combantant2 = &pool[betterRand(g_numShips-1)+1];
    if(combantant1 == NULL || combantant2 == NULL)
    {
      i--;//LAME
      continue;
    }

    if(combantant1->finalFitness < combantant2->finalFitness)
    {
      toDie = combantant1;
    }
    else
    {
      toDie = combantant2;
    }
    logging(LOG_FITNESS ,"DEATH SELECTION! %s(%f) vs. %s(%f)... %s is WIPED\n",combantant1->name,combantant1->finalFitness,combantant2->name,combantant2->finalFitness, toDie->name);

    //Make a baby in the dead guy's slot
    dadCompetitor1 = &pool[betterRand(g_numShips-1)+1];
    dadCompetitor2 = &pool[betterRand(g_numShips-1)+1];
    if(dadCompetitor1 == NULL || dadCompetitor2 == NULL)
      return;
    if(dadCompetitor1->finalFitness > dadCompetitor2->finalFitness)
      dad = dadCompetitor1;
    else
      dad = dadCompetitor2;
    logging(LOG_FITNESS ,"SELECTION! %s(%f) vs. %s(%f)",dadCompetitor1->name,dadCompetitor1->finalFitness,dadCompetitor2->name,dadCompetitor2->finalFitness);
    momCompetitor1 = &pool[betterRand(g_numShips-1)+1];
    momCompetitor2 = &pool[betterRand(g_numShips-1)+1];
    if(momCompetitor1 == NULL || momCompetitor2 == NULL)
      return;
    if(momCompetitor1->finalFitness > momCompetitor2->finalFitness)
      mom = momCompetitor1;
    else
      mom = momCompetitor2;

    logging(LOG_FITNESS ,"   gets with    %s(%f) vs. %s(%f)\n",dad->name,dad->finalFitness,mom->name,mom->finalFitness);

    selectionDisplay(combantant1,combantant2,dadCompetitor1,dadCompetitor2,momCompetitor1,momCompetitor2,dad,mom,toDie);

    breedShips(dad, mom, toDie);

  }
}


void printDNA(struct ships* ship)
{
  int i;
  logging(LOG_FITNESS,"%s:\t",ship->name);
  for(i=0; i < DNA_SIZE; i++)	
  {
    logging(LOG_FITNESS,"%4d %4.1f %4.1f,",ship->dna[i].time, ship->dna[i].xThrust, ship->dna[i].yThrust);
  }
  logging(LOG_FITNESS,"\n");
}

void fprintDNA(FILE* f, struct ships* ship)
{
  int i;
  fprintf(f,"%s:\t",ship->name);
  for(i=0; i < DNA_SIZE; i++)	
  {
    fprintf(f,"%4d %4.1f %4.1f,",ship->dna[i].time, ship->dna[i].xThrust, ship->dna[i].yThrust);
  }
  fprintf(f,"\n");
}


//TODO could play around here. This is a pretty naive method.
void breedShips(struct ships* dad, struct ships* mom, struct ships* baby)
{
  int blendPoint;
  int i, j;

  //and then blend that DNA
  blendPoint = betterRand(DNA_SIZE);
  for(i=0; i < DNA_SIZE; i++)	
  {
    if(i < blendPoint)
    {
      baby->dna[i].time = dad->dna[i].time;
      baby->dna[i].xThrust = dad->dna[i].xThrust;
      baby->dna[i].yThrust = dad->dna[i].yThrust;
    }
    else
    {
      baby->dna[i].time = mom->dna[i].time;
      baby->dna[i].xThrust = mom->dna[i].xThrust;
      baby->dna[i].yThrust = mom->dna[i].yThrust;
    }
  }
  genName(baby->name);
  baby->fitness = 0;
  baby->finalFitness = 0;
  for(j=0; j<20; j++)
  {
    baby->mutations[j].mutationType = -1;
    baby->mutations[j].mutationLocation = -1;
    baby->mutations[j].mutationSize = 0;
  }

  //If the parents are identical, we should throw in extra mutations. You know, inbreeding makes for freaks. Also, it keeps the population from all being clones.
  if(dad->finalFitness == mom->finalFitness)  //Easy method, a more generic solution would be to actually diff the DNA
  {
    mutateShip(baby);
    mutateShip(baby);
    mutateShip(baby);
    mutateShip(baby);
    mutateShip(baby);
  }
  mutateShip(baby);
  g_newborn = baby;

  logging(LOG_FITNESS,"%s and %s breed: %s\n",dad->name, mom->name, baby->name);

  //printDNA(dad);
  //printDNA(mom);
  //printDNA(baby);

  //Hmmm, it essentially halts here. Not ideal.
  breedDisplay(dad, mom, baby, blendPoint);

}

void mutationName(char *name, int mutationType)
{
  switch(mutationType)
  {
    case 0:
      sprintf(name,"Tweak Time");
      break;
    case 1: 
      sprintf(name,"Tweak X Thrust");
      break;
    case 2: 
      sprintf(name,"Tweak Y Thrust");
      break;      
    case 3: //set Random
      sprintf(name,"Randomize Time");
      break;
    case 4: //set Random
      sprintf(name,"Randomize X Thrust");
      break;
    case 5: //set Random
      sprintf(name,"Randomize Y Thrust");
      break;
    case 6: //Section scrambled
      sprintf(name,"Scramble Section");
      break;
    case 7: //completely different
      sprintf(name,"Completely different");
      break;
    case 8: //Copy
      sprintf(name,"Copy Section");
      break;
    case 9: //Insert a small adjustment
      sprintf(name,"Insert small adjust");
      break;
    case 10: //Zero the thrust, useful now that we judge fitness on fuel costs
      sprintf(name,"Zero Thrust");
      break;  

    default:
      return;
  }
}

void recordMutation(struct ships* ship, int mutationType, int mutationLocation, int mutationSize)
{
  int i;
  for(i=0; i<20; i++)
  {
    if(ship->mutations[i].mutationType == -1)
    {
      ship->mutations[i].mutationType = mutationType;
      ship->mutations[i].mutationLocation = mutationLocation;
      ship->mutations[i].mutationSize = mutationSize;
      return;
    }
  }
}

//And not to get too meta, but an evolving method of mutating my agents would be pretty hip.
void mutateShip(struct ships* ship)
{
  int i, sectionStart, offset, insertionPoint;
  int numMutations = betterRand(g_mutationRate);  //TODO ugh, this is something that needs to be played with
  int mutationType;
  int mutationLocation;
  int sectionSize;

  while(numMutations--)
  {
    mutationType = betterRand(MUTATION_TYPES);
    mutationLocation = betterRand(DNA_SIZE);
    sectionSize = 1;

    switch(mutationType)
    {
      case 0: //Tweak timing,  by far the most useful mutation... hmmm...
        ship->dna[mutationLocation].time *= ((betterRand(4000))-2000.0)/1000.0;  // multiply by -2 to 2
        break;
      case 1: //Tweak x thrust
        ship->dna[mutationLocation].xThrust *= ((betterRand(4000))-2000.0)/1000.0;  // multiply by -2 to 2
        break;
      case 2: //Tweak y timing
        ship->dna[mutationLocation].yThrust *= ((betterRand(4000))-2000.0)/1000.0;  // multiply by -2 to 2
        break;      

      case 3: //set Random
        ship->dna[mutationLocation].time = rand();
        break;
      case 4: //set Random
        ship->dna[mutationLocation].xThrust = rand() ;
        break;
      case 5: //set Random
        ship->dna[mutationLocation].yThrust = rand();
        break;

      case 6: //Section scrambled
        sectionSize = rand()%DNA_SIZE;
        for(sectionStart = mutationLocation, i=0; i<sectionSize; i++,sectionStart++)
        {
          ship->dna[sectionStart%DNA_SIZE].time = betterRand(DEFAULT_TIME_SECTION);
          ship->dna[sectionStart%DNA_SIZE].xThrust = rand();			
          ship->dna[sectionStart%DNA_SIZE].yThrust = rand();			
        }
        break;

      case 7: //completely different
        generateRandStrand(ship->dna);
        mutationLocation = 0;
        sectionSize = DNA_SIZE;
        break;

      case 8: //Copy
        sectionSize = betterRand(DNA_SIZE);
        offset = betterRand(DNA_SIZE);
        for(sectionStart = betterRand(DNA_SIZE), i=0; i<sectionSize; i++,sectionStart++)
        {
          ship->dna[sectionStart%DNA_SIZE].time = ship->dna[(sectionStart+offset)%DNA_SIZE].time;
          ship->dna[sectionStart%DNA_SIZE].xThrust = ship->dna[(sectionStart+offset)%DNA_SIZE].xThrust;
          ship->dna[sectionStart%DNA_SIZE].yThrust = ship->dna[(sectionStart+offset)%DNA_SIZE].yThrust;
        }
        break;

      case 9: //Insert a small adjustment
        insertionPoint = 1+ betterRand(DNA_SIZE-1);
        mutationLocation = insertionPoint;
        for( i=DNA_SIZE-1; i > insertionPoint; i--)
        {
          ship->dna[i].time = ship->dna[i-1].time;
          ship->dna[i].xThrust = ship->dna[i-1].xThrust;
          ship->dna[i].yThrust = ship->dna[i-1].yThrust;        
        }
        ship->dna[insertionPoint].time = betterRand(DEFAULT_TIME_SECTION);
        ship->dna[insertionPoint].xThrust = rand();			
        ship->dna[insertionPoint].yThrust = rand();	
        break;

      case 10: //Zero the thrust, useful now that we judge fitness on fuel costs
        ship->dna[mutationLocation].yThrust = 0;
        ship->dna[mutationLocation].xThrust = 0;
        break;  

      default:
        ;//No mutation

    }
    recordMutation(ship, mutationType, mutationLocation, sectionSize);
  }

}

//3*(1/26) odds of a collision that will clobber the original.   hmmm, 11%
void mutateName(char* name)
{
  //pick on char within name, change
  name[betterRand(strlen(name))] = "abcdefghijklmnopqrstuvwxyz"[betterRand(26)]; //Totally safe. Trust me.

  //Man, fuck you MS.
  if( strcmp(name,"con") == 0 || 
      strcmp(name,"prn") == 0 || 
      strcmp(name,"aux") == 0 || 
      strcmp(name,"nul") == 0 )
  {
    sprintf(name,"wtf");
  }
}

void copyDNA(struct DNA from[], struct DNA to[])
{
  int i;
  for(i=0; i < DNA_SIZE; i++)	
  {
    to[i].time = from[i].time;
    to[i].xThrust = from[i].xThrust;
    to[i].yThrust = from[i].yThrust;
  }

}


int currentDNAindex(struct ships ship)
{
  int sumTime=0;
  int i=0;
  for(i=0; sumTime < g_time && i < DNA_SIZE; i++)
    sumTime += ship.dna[i].time;
  return i;
}


