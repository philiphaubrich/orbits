#ifndef __GPFIGHT__
#define __GPFIGHT__
/* 	basic.h
    9/14/11 By Philip Haubrich
	gpfight.h 
	4/10/12 By Philip Haubrich
  changed to orbit.h
  3/10/13  By Philip Haubrich    My god, it's been a year?
  

*/
#include "globals.h"

typedef enum
{
	LOG_NONE = 0x00,
	LOG_ALWAYS = 0xFFFF,
	LOG_ERR = 0x0001,
	LOG_MIDGAME = 0x0004, //mid-round game actions, 
	LOG_FITNESS = 0x0008, //fitness functions, Detailing who gets selected, who breeds, who gets born whatnot
	LOG_HIGHGAME = 0x0010, //Higher-level game info. Rounds, who gets loaded, who gets deleted, 
	LOG_VERBOSE = 0x0020 //Eh, everything else
}logging_types;


//Eh, it'll change later, but for now, simple
//If you do change it, you'll have to dink with the shadow-ship portions again
struct DNA
{
  int time;       // in plank-time.  how long this action is going to be performed.
  float xThrust;  // From -1.0 to 1.0  Full back to full forwards
  float yThrust;  // From -1.0 to 1.0  Full back to full forwards
};

struct MUTATIONDATA
{
  int mutationType;
  int mutationLocation;
  int mutationSize;
};

struct ships
{
  //Stay between rounds
  char name[SHIPNAMESIZE];
  float fuel;
  uint32 color;
  struct MUTATIONDATA mutations[20]; //records what mutations a baby has at what point in their DNA.  So we can see if it helped or hurt
  struct DNA dna[DNA_SIZE];
  float finalFitness; //Once you know this you don't REALLY need to process it again, unless you want to see it in action. This is essentially a "skip me" flag when running without a display
  float finalFuel;
  uchar finalCrashed;   //So I'm adding these other final states prettymuch so my metrics know what's up when it doesn't actually crunch the numbers
  uchar finalExhausted;
  
  //round variables
  float x, y;
  float velx, vely;
  float accelx, accely;
  uchar crashed;      // if they hit a planet
  uchar exhausted;    // if they exhaust their DNA. The last command never times out.
  int planetCrashed; //which planet they crashed into, so it looks better if I can move their corpse
  float fitness;
  float minDist[NUM_PLANETS];
};


//constant objects, sun, planets and such.   Shit to orbit each other.  Big shit.
//0 is special, it's the sun. 
struct planets
{
  float x, y;
  float mass;
  float size;
  float dist;
  int orbiting;
  float degree; 
  uint32 color;
};


//Just to make it pretty
//An array with every 10/100 positions will display nicely.
struct path
{
  float x;
  float y;
};


void processArguments(int argc, char** argv);
void initRound();
void systemInit(int seed);
void shipInit(int seed);
void genName(char name[SHIPNAMESIZE]);
void generateRandStrand(struct DNA* strand);
void resetShip();

int endRound();
void paintScreen();
void applyInput(int in);
void applyInput_UserControl(int in);
char applyInput_CursorControl(int in, int* x, int* y);
void userObserve();

int logging(int type, const char* format, ...);

void generateSystem();
void updateOrbits();

void moveShips();
void steerShips();

void copyPath( struct path* oldKingsPath, struct path* newKingsPath);

float distance(float x1, float y1, float x2, float y2);

int betterRand(int n); //Could go anywhere really utility function



#endif 


