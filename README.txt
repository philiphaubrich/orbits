Built in Mingw  http://www.mingw.org/   and gcc

Dependencies:
SDL			http://www.libsdl.org/
SDL_ttf			http://www.libsdl.org/projects/SDL_ttf/
libfreetype-6		uh

Windows Build:
make clean 
make
./orbit.exe

Linux Build:
make -f easyMake
./orbit

Arguments:
[--help][-h] prints this and exits.
[-r numRounds]	# of rounds to process. Default: 10, 0 for infinite
[-l roundLength] # of turns in a round. Default: 20000
[-f] Run fast. Limited only by your processor. 
[-s] Run slow. Turn is limited to .25 seconds. 
[-d] Run dark. No input or output. Runs really fast though.  
[-m] Mute. Log NOTHING. For running overnight.
[-v] Verbose. Log EVERYTHING.  See also globals.h for finer control.
[-e] Ship random number generator seed
[-p] Planet random number generator seed
[-n] Number of ships, the size of the population pool
