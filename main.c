/* Philip Haubrich 
 *
 * TODO: Selecting for the closest path AND fuel has never really worked well. -DUSINGFUEL is experimental. 
 *
 */


//The headers
#include <time.h>
#include <unistd.h>
#include <getopt.h>
#include <math.h>
#include "SDL2/SDL.h"
//#include "SDL/SDL_image.h"
#include "SDL2/SDL_ttf.h"

#include "orbit.h"
#include "gplib.h"
#include "display.h"
#include "globals.h"






//The event structure
SDL_Event g_event;

//String buffer for all the text. Fuck it, it's a demo
char g_buff[BUFFSIZE] = " ";
int g_curserPos=0;

char g_bugBuff[100]= " ";
float g_debug=1;


struct ships shipArray[MAX_NUM_SHIPS];
struct planets  planetArray[NUM_PLANETS];

struct path oldKingsPath[ROUND_LENGTH_DEFAULT/PATHPRECISION]; //pretty lame use of arrays
struct path newKingsPath[ROUND_LENGTH_DEFAULT/PATHPRECISION];

int g_numShips = DEFAULT_NUM_SHIPS;
int g_shipSeed = 0;
int g_planetSeed = 0;
int g_time = 0;
int g_round = 0;	
int g_paused = 1;
int g_topFit = 0;
int g_next = -1;
int g_nextImprovement = 0;
int g_viewFitArray = 0;
int g_previousTimeMark = 0;
int g_displaySelection = 0;

//This is for logging the players actions
int g_playerActionIndex=0;

const clock_t QUARTERSEC = CLOCKS_PER_SEC/4;
const clock_t MILLISEC = CLOCKS_PER_SEC/1000;
const clock_t NO_DISPLAY = -2;
const clock_t MIN_DISPLAY = -1;
const clock_t FASTEST = 0;
clock_t g_speed = 0; // -d noDisplay  -f fastest (no delay) -s
int g_numRounds = NUM_ROUNDS_DEFAULT;	// -r
int g_roundLength = ROUND_LENGTH_DEFAULT;	// -l
int g_logging = LOGGING_DEFAULT;	





//Logging
FILE* f_log;
FILE* f_mutationchart;
FILE* f_fitchart;


int init()
{

  //SDL_WM_SetCaption( "GA Orbits", NULL );
  if(!displayInit())
  {
    return false;
  }


  //Yes, this simulation is weak against timing attacks 
  if(g_planetSeed == 0)
  {
    g_planetSeed = abs(time(NULL));
  }

  systemInit(g_planetSeed);

  if(g_shipSeed == 0)
  {
    g_shipSeed = abs(time(NULL));
  }
  shipInit(g_shipSeed);

  //Then seed it for the rest of the actions:
  srand(g_planetSeed + g_shipSeed);

  if( load_files() == -1 )
  {
    return false;
  }

  f_log = fopen("log.txt", "wa");
  f_mutationchart = fopen("MutationChart.txt", "wa");
  fprintf(f_mutationchart,"TweakTime, TweakX, TweakY, N/A, RandX, RandY, SectionScramble, CompletelyDifferent, CopySection, Insert, zeroThrust  :\n");

  f_fitchart = fopen("fitChart.txt", "wa");
  fprintf(f_fitchart,"maxFit, fuel\n");


  return true;
}


const char help[]="\
                   [--help][-h] prints this and exits.\n\
                   [-r numRounds]	# of rounds to process. Default: 10, 0 for infinite\n\
                   [-l roundLength] # of turns in a round. Default: 20000\n\
                   [-f] Run fast. Limited only by your processor. \n\
                   [-s] Run slow. Turn is limited to .25 seconds. \n\
                   [-d] Run dark. No input or output. Runs really fast though.  \n\
                   [-m] Mute. Log NOTHING. For running overnight.\n\
                   [-v] Verbose. Log EVERYTHING.  See also globals.h for finer control.\n\
                   [-e] Ship random number generator seed\n\
                   [-p] Planet random number generator seed\n\
                   [-n] Number of ships, the size of the population pool\n";
void processArguments(int argc, char** argv)
{
  int c;	//Damn you, you terse C motherfuckers and your shitty terse examples

  opterr = 0;

  while ((c = getopt (argc, argv, "hr:l:sfdmvp:e:n:")) != -1)
  {
    switch (c)
    {
      case 'h':	//help
        printf("%s",help);
        exit(0);
        break;
      case 'r':	//Num Rounds
        sscanf(optarg,"%d",&g_numRounds);
        if(g_numRounds == 0)
          g_numRounds = 2147483647;
        break;
      case 'l':	//Round length
        sscanf(optarg,"%d",&g_roundLength);
        break;
      case 's':	//slow
        g_speed = QUARTERSEC;
        break;
      case 'f':	//fast
        g_speed = FASTEST;
        break;
      case 'd':	//dark, no display  (superfast)
        g_speed = NO_DISPLAY;
        g_paused = 0; //make sure we don't wait for IO that's never coming.
        break;
      case 'm':
        g_logging = 0; //clears all logging options
        break;
      case 'v':
        g_logging = LOG_ALWAYS; //verbose, log EVERYTHING
        break;
      case 'p': //Planet seed?
        sscanf(optarg,"%d",&g_planetSeed);
        break;
      case 'e': //ship sEed, I guess?
        sscanf(optarg,"%d",&g_shipSeed);
        break;
      case 'n': //number of ships
        sscanf(optarg,"%d",&g_numShips);
        if(g_numShips > MAX_NUM_SHIPS)
          g_numShips = MAX_NUM_SHIPS;
        break;

      case '?':
        if (optopt == 'c')
        {
          fprintf(stderr, "Option -%c requires an argument.\n", optopt);
        }
        /*else if (isprint (optopt))
          {
          fprintf (stderr, "Unknown option `-%c'.\n", optopt);
          }*/
        else
        {
          fprintf (stderr,"Unknown option character `%x(%c)\n", optopt, optopt);
        }
        exit(1);
      default: 
        return;
    }
  }
}


void updatePlayerAction()
{  
  shipArray[0].dna[g_playerActionIndex].time = g_time - g_previousTimeMark;

  g_previousTimeMark = g_time;
  g_playerActionIndex++;
  shipArray[0].dna[g_playerActionIndex].xThrust = shipArray[0].accelx;
  shipArray[0].dna[g_playerActionIndex].yThrust = shipArray[0].accely;

}



//Called in Main
int handleKeyStrokeUp()
{
  int sym = g_event.key.keysym.sym;

  switch(sym)
  {
    /*
       case 'w':
       shipArray[0].accely = 0;
       updatePlayerAction();
       break;
       case 'a':
       shipArray[0].accelx = 0;
       updatePlayerAction();
       break;
       case 's':
       shipArray[0].accely = 0;
       updatePlayerAction();
       break;
       case 'd':
       shipArray[0].accelx = 0;
       updatePlayerAction();
       break;
       */
    default:
      ;
  }

  return 0;
}
//Called in Main
int handleKeyStrokeDown()
{
  //char c;
  int sym = g_event.key.keysym.sym;
  //int mod = g_event.key.keysym.mod;

  switch(sym)
  {
    case SDLK_ESCAPE:
      cleanUp();
      exit(0);
      /*
         case 'w':
         shipArray[0].accely = -100;
         updatePlayerAction();
         break;
         case 'a':
         shipArray[0].accelx = -100;
         updatePlayerAction();
         break;
         case 's':
         shipArray[0].accely = 100;
         updatePlayerAction();
         break;
         case 'd':
         shipArray[0].accelx = 100;
         updatePlayerAction();
         break;
         */

      //Wow, well that's a fun showcase. Run the thing, explain what's going on, kick it to the slowest and specifically explain all the math that's being performed: The orbit being calculated, gravity being applied from all planets to all ships O(n^2), and each ship thrusting according to their plan. Then kick it onto the fastest setting explaining that this is as fast as the computer can handle it. There is no delay.   Then switch it over the dark mode. Saying that you've got it to go even faster, by cutting out the effort of painting the screen. All the math is still being applied. Then cut it back to slowest and show them the round. Yeah, that's right. The vast majority of the work being done is in the graphics. And these are not serious graphics by a far shot. It's not that it takes SOO long to paint the screen, it's that the math being done, while moderately impressive to humans, is NOTHING to a machine. Computers gotta compute.
    case '1':	//slow
      g_speed = QUARTERSEC;
      break;
    case '2':
      g_speed = MILLISEC;
      break;
    case '3':	//fast
      if(g_speed == NO_DISPLAY || g_speed == MIN_DISPLAY)
      {
        g_next = FASTEST; // Testing
      }
      else
      {
        g_speed = FASTEST;
        g_next = -1;
      }
      break;
    case '4': 
      g_speed = MIN_DISPLAY;
      break;
    case '5':	//dark, no display  (superfast)
      g_speed = NO_DISPLAY;
      g_paused = 0; //make sure we don't wait for IO that's never coming.
      break;

    case 'n': //next round
      g_speed = NO_DISPLAY;
      g_next = QUARTERSEC;
      break;

    case 'b': //next round that has an improvement, the next (b)etter round
      g_speed = MIN_DISPLAY;
      g_nextImprovement = 1;
      break;

    case 'v':
      g_viewFitArray = ~g_viewFitArray;
      break;

    case 's':  //It's for "selection" sure.
      g_displaySelection = ~g_displaySelection;
      break;

    default:
      ;
  }
  return 0;
}

int handleEvent(int eventType)
{
  static int wheel;
  switch(eventType)
  {
    case SDL_QUIT:
      cleanUp();
      exit(0);
      return true;
      break;

    case SDL_KEYDOWN:
      handleKeyStrokeDown();
      break;

    case SDL_KEYUP:
      handleKeyStrokeUp();
      break;

    case SDL_MOUSEBUTTONDOWN:
      if( g_event.button.button == 4) //mousewheel down
      {
        wheel--;
      }
      if( g_event.button.button == 5) //mousewheel up
      {
        wheel++;
      }
      //g_debug = wheel;

    default:
      ;
  }
  return false;
}

//TEST ME
void apocalypse(struct ships* survivor)
{
  struct DNA survivor_dna[DNA_SIZE];
  int i;
  //Save survivor
  for(i=0; i< DNA_SIZE; i++)
  {
    survivor_dna[i].time = survivor->dna[i].time;
    survivor_dna[i].xThrust = survivor->dna[i].xThrust;
    survivor_dna[i].yThrust = survivor->dna[i].yThrust;
  }

  shipInit(g_shipSeed);

  for(i=0; i< DNA_SIZE; i++)
  {
    shipArray[0].dna[i].time = survivor_dna[i].time;
    shipArray[0].dna[i].xThrust = survivor_dna[i].xThrust;
    shipArray[0].dna[i].yThrust = survivor_dna[i].yThrust;
  }

}


//I may, perhaps, use too many globals...  Still though, git'r'done
void stateControl()
{
  static float oldfit=0.0;

  //skipping to the start of the next round
  if(g_next != -1)
  {
    g_speed = g_next;
    g_next = -1;
  }
  if(g_nextImprovement == 1)
  {
    if(oldfit < g_maxFit)
    {
      oldfit = g_maxFit;
      g_speed = QUARTERSEC;
      g_nextImprovement = 0;
    }
  }
}

void endgame()
{
  FILE* f_winner;
  char filename[SHIPNAMESIZE];
  sprintf(filename,"winner.txt");
  f_winner = fopen(filename, "wa");

  fprintf(f_winner,"planet Seed: %d\n", g_planetSeed);
  fprintf(f_winner,"ship Seed: %d\n", g_shipSeed);
  fprintf(f_winner,"NUM_PLANETS: %d\n", NUM_PLANETS);
  fprintf(f_winner,"g_numShips: %d\n", g_numShips);
  fprintf(f_winner,"DNA_SIZE: %d\n", DNA_SIZE);
  fprintf(f_winner,"MUTATION_TYPES: %d\n", MUTATION_TYPES);
  fprintf(f_winner,"NUM_PLANETS: %d\n\n", NUM_PLANETS);

  fprintf(f_winner,"number of rounds: %d\n", g_round);
  fprintf(f_winner,"cycles spent:%d\n", (int)clock());


  fprintDNA(f_winner,g_kingoftheHill);


  cleanUp();
  exit(0);
}

// MAIN!
int main( int argc, char* args[] )
{
  int quit = false;
  static int oldTime = 0;

  processArguments(argc, args);

  if( init() == false )
  {
    return 1;
  }

  while( quit == false ) //Main loop
  {
    initRound();  
    while(g_time < g_roundLength) //one generation
    {

      while( SDL_PollEvent( &g_event ) )
      {
        quit = handleEvent(g_event.type);
      }

      if( SDL_GetTicks() - oldTime > g_speed || g_speed == NO_DISPLAY || g_speed == MIN_DISPLAY)
      { 
        g_time++;
        updateOrbits();
        steerShips();
        moveShips();
        updateFitness(shipArray, planetArray);
        oldTime = SDL_GetTicks();
      }
      if(g_speed != NO_DISPLAY)
      {
        animation();
      }
    }

    selection(shipArray);
    chartLog();

    //test if we're good enough
    if(g_kingoftheHill != NULL && g_kingoftheHill->finalFitness > GOOD_ENOUGH)
    {
      //Uh... end? 
      //endgame();    //Or no?
    }

    stateControl();
  }
  cleanUp();

  return 0;
}
