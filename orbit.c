/*
Built off of GP fighters

Open source, GPLv3
Signed in blood, -Philip Haubrich, June 12th 2012

Using it to see if we can conquer orbital mechanics. Wish me luck.


*/

#include "orbit.h"
#include "gplib.h"
#include "globals.h"

#include "SDL/SDL.h"

#include <stdlib.h>
#include <time.h>
#include <unistd.h>
#include <stdio.h>
#include <math.h>

//A lot of these externs should probably be in globals.h
extern struct ships shipArray[MAX_NUM_SHIPS];
extern struct planets  planetArray[NUM_PLANETS];
extern struct path newKingsPath[ROUND_LENGTH_DEFAULT/100];
extern struct path oldKingsPath[ROUND_LENGTH_DEFAULT/100];
extern struct ships* g_kingoftheHill;

/*
extern int g_planetSeed;
extern int g_time ;
extern int g_round ;
extern int g_playerActionIndex;
extern int g_previousTimeMark;

extern clock_t g_speed;
extern int g_numRounds;
extern int g_roundLength;
extern int g_logging;
extern int g_numShips;

//Logging
extern FILE* f_log;
*/

const float GRAV_CONST = 6.667;  //well this is meaningless, but a nice thing to play with





void initRound()
{
  logging(LOG_HIGHGAME, "\nRound: %d\n",g_round);
  g_round++;
  g_time = 0;

  systemInit(g_planetSeed);

  //Move the player's recorded actions in ship 0 to the shadowShip 1
  copyDNA(shipArray[0].dna,shipArray[1].dna);
  g_playerActionIndex = 0;
  shipArray[1].dna[0].xThrust = 0;
  shipArray[1].dna[0].yThrust = 0;
  g_previousTimeMark = 0;

  copyPath(oldKingsPath, newKingsPath);

  resetShip();

  return;
}

//On "books suck, get a nook". Anything worth reading should be on the internet. Anything worth reading later should be printed in a book. -4/12/12


//HA! originally I had full gravity simulation in here, and then I came to my senses and realized it's a simulation.
void updateOrbits()
{
  int i,j;
  float oldx, oldy;
  float deltax, deltay;
  //Skipping the sun, it don't move.
  for(i=1; i< NUM_PLANETS; i++)
  {
    planetArray[i].degree += 5 / planetArray[i].dist;  //Shrug?
    if( planetArray[i].degree >360.0)
      planetArray[i].degree -= 360.0;
    oldx = planetArray[i].x;
    oldy = planetArray[i].y;
    planetArray[i].x = SCREEN_WIDTH/2 + planetArray[i].dist * sin(planetArray[i].degree /RADIAN_TO_DEGREE );
    planetArray[i].y = SCREEN_HEIGHT/2 + planetArray[i].dist * cos(planetArray[i].degree /RADIAN_TO_DEGREE );
    deltax = planetArray[i].x - oldx;
    deltay = planetArray[i].y - oldy;

    for(j=0; j< g_numShips; j++)
    {
      if(shipArray[j].crashed && shipArray[j].planetCrashed == i)
      {
        shipArray[j].x += deltax;
        shipArray[j].y += deltay;
      }
    }
  }
}



void systemInit(int seed)
{
  int i;
  unsigned int seedStorage;
  uint32 red, green, blue;

  planetArray[0].x = SCREEN_WIDTH/2;
  planetArray[0].y = SCREEN_HEIGHT/2;
  planetArray[0].mass = 1000;
  planetArray[0].size = SCREEN_HEIGHT/20;
  planetArray[0].dist = 0;
  planetArray[0].orbiting = 0;
  planetArray[0].degree = 0;
  planetArray[0].color = 0x00ffff00; //yellow

  seedStorage = rand();
  seedStorage = seedStorage <<16 | rand();
  srand(seed);
  for(i=1; i<NUM_PLANETS; i++)
  {
    planetArray[i].mass = 20+ betterRand(100);
    planetArray[i].size = planetArray[i].mass/10;
    planetArray[i].dist = SCREEN_HEIGHT/20 + betterRand(SCREEN_HEIGHT/2);
    planetArray[i].orbiting = 0;        //Yeah, all orbiting the sun right now
    planetArray[i].degree = betterRand(360);
    planetArray[i].x = SCREEN_WIDTH/2 + planetArray[i].dist * sin(planetArray[i].degree );
    planetArray[i].y = SCREEN_HEIGHT/2 + planetArray[i].dist * cos(planetArray[i].degree );
    red = betterRand(256);
    green = betterRand(256-red);
    blue = 256 - (red+green);
    planetArray[i].color = red<<16 | green<<8 | blue; 
  }

  srand(seedStorage); // Yeah, this doesn't go so well. Forms a loop
  //srand(abs(time(NULL))); //This is also pretty bad, as we lose our starting point.

  //Debugging our seedstorage
  i=rand();
  i=rand();
  i=rand();

}

void shipInit(int seed)
{ 
  int i,j;
  uint32 r,g,b;

  srand(seed);

  for(i=0; i<g_numShips; i++)
  {
    r = 64+betterRand(192);
    g = 64+betterRand(192);
    b = 64+betterRand(192);

    genName(shipArray[i].name);
    shipArray[i].color = r<<16 | g<<8 | b;
    shipArray[i].finalFitness = 0;
    shipArray[i].finalFuel = 0;
    shipArray[i].finalCrashed = 0;
    shipArray[i].finalExhausted = 0;
    generateRandStrand((shipArray[i].dna));
    //Clearing metrics
    for(j=0; j< 20; j++) //Shut up there are 20
    {
      shipArray[i].mutations[j].mutationType = -1;
      shipArray[i].mutations[j].mutationLocation = -1;
      shipArray[i].mutations[j].mutationSize = 0;
    }
  }
  /* We're not doing that.
  // the player's ship
  shipArray[0].color = 0x00ffffff;
  // the shadow ship
  shipArray[0].color = 0x00999999;
  */

  resetShip();
}

void resetShip()
{
  int i,j; 

  for(i=0; i<g_numShips; i++)
  {  
    shipArray[i].crashed = 0;
    shipArray[i].x = SCREEN_WIDTH/2;
    shipArray[i].y = SCREEN_HEIGHT-10;
    shipArray[i].velx = 5000.0;
    shipArray[i].vely = -1000.0;
    shipArray[i].accelx = 0;
    shipArray[i].accely = 0;
    shipArray[i].fuel = SHIPFUEL; //FILL 'ER UP!

    shipArray[i].fitness = 0; 
    shipArray[i].exhausted = 0;
    for(j=0; j<NUM_PLANETS; j++)
    {
      shipArray[i].minDist[j] = 10000000.0; //arbitrary large number
    }
  }
}


void copyPath( struct path* oldKingsPath, struct path* newKingsPath)
{
  int i;
  for(i=0; i< g_roundLength/PATHPRECISION; i++)
  {
    oldKingsPath[i].x = newKingsPath[i].x;
    oldKingsPath[i].y = newKingsPath[i].y;

  }

}

void generateRandStrand(struct DNA* strand)
{
  int i = 0;
  if(strand == NULL)
  {
    logging(LOG_ERR,"ERR, strand's blank dude.   Dude.\n");
  }
  for(i=0; i< DNA_SIZE; i++)
  {
    strand[i].time = betterRand(DEFAULT_TIME_SECTION); 
    strand[i].xThrust = betterRand(20)-10 ;
    strand[i].yThrust = betterRand(20)-10 ;
  }
}

void genName(char name[SHIPNAMESIZE])
{
  const char consonant[20] = "bcdfghjklmnpqrstvwxz";
  const char vowel[6] = "aeiouy";

  //Rand consonant
  //Rand Vowel
  //Rand consonant
  name[0] = consonant[betterRand(20)];
  name[1] = vowel[betterRand(6)];
  name[2] = consonant[betterRand(20)];
  name[3] = 0;

  //Banned names in windows apparently: con prn aux nul
  //Man, fuck you windows. I'm not sending them to con, I'm sending them to con.agent
  if( strcmp(name,"con") == 0 || 
      strcmp(name,"prn") == 0 || 
      strcmp(name,"aux") == 0 || 
      strcmp(name,"nul") == 0 )
  {
    sprintf(name,"wtf");
  }
}
//First name ver made: "lem"



void steerShip(struct ships* target)
{
  int j;
  int timeCount=0;
  j=0;

  if(target == NULL) return;

  timeCount = target->dna[0].time;

  while(timeCount < g_time && j < DNA_SIZE-1)
  {
    j++;
    timeCount += target->dna[j].time;
  }
  if(j == DNA_SIZE)
  {
    target->exhausted = 1;
  }

  //j is now current action in dna
  target->accelx = target->dna[j].xThrust;
  target->accely = target->dna[j].yThrust;
}


void steerShips()
{
  int i;

  if(g_speed == NO_DISPLAY || g_speed == MIN_DISPLAY)
  {
    steerShip(g_newborn);
    return; //Yer Done, 
  }

  for(i=0; i<g_numShips; i++)
  {
    steerShip(&shipArray[i]);
  }
}


float between(float x, float min, float max)
{
  if(x < min)
    return min;
  if(x > max)
    return max;
  return x;
}



void moveShip(struct ships* target)
{
  int j;
  float force, xforce, yforce;
  float dist, distSquared, xDelta, yDelta;

  if(target == NULL)
    return;

  //for(i=0; i<g_numShips; i++) OPTIMIZED! BITCHES!
  {	  

    if(target->crashed == 1)
      return;

    xforce = between(target->accelx, -10.0, 10.0);
    yforce = between(target->accely, -10.0, 10.0);

    //Check to see if they're out of fuel
    if(target->fuel < 0)
    {
      xforce = 0;
      yforce = 0;
    }
    else
      target->fuel -= (abs(xforce)+abs(yforce));

    for(j=0; j<NUM_PLANETS; j++)
    {
      xDelta = target->x - planetArray[j].x;
      yDelta = target->y - planetArray[j].y;
      distSquared = xDelta*xDelta + yDelta*yDelta;
      dist = sqrt(distSquared);
      //G* M1 * M2 / r*r
      force = GRAV_CONST * SHIPMASS * planetArray[j].mass / (distSquared);
      xforce -= force * xDelta / dist;
      yforce -= force * yDelta / dist;

      if(dist < planetArray[j].size)
      {
        target->crashed = 1;
        target->planetCrashed = j;
      }

    }
    //f = ma    
    //f = m v/t  or   v = tf/m
    //assuming mass = 1 and time = 1   v = f
    target->velx += xforce;
    target->vely += yforce;

    // dist = v*t
    target->x += target->velx /100000.0 ; //So yeah, we apparently need something to keep this is the scope of our screensize...
    target->y += target->vely /100000.0 ;

    // if the ship is currently king, record his path
    if(target == g_kingoftheHill && g_time % 100 == 0 && g_time/100 < ROUND_LENGTH_DEFAULT/PATHPRECISION)
    {
      newKingsPath[g_time/100].x = g_kingoftheHill->x;
      newKingsPath[g_time/100].y = g_kingoftheHill->y;
    }
  }
  //sprintf(g_bugBuff,"%3.1f %3.1f %3.1f", shipArray[0].x, shipArray[0].velx, shipArray[0].accelx);
  //sprintf(g_bugBuff,"%3.1f", shipArray[0].accelx);
}


void moveShips()
{
  int i;

  if(g_speed == NO_DISPLAY || g_speed == MIN_DISPLAY)
  {
    moveShip(g_newborn);
    return; //Yer Done, we don't actually need to process any ships other than the new unknown one 
  }

  for(i=0; i<g_numShips; i++)
  {	  
    moveShip(&shipArray[i]);
  }
}  



//Profiled as #1 resource hog at about 40%.   Not much to do about it.  
float distance(float x1, float y1, float x2, float y2)
{
  float deltaX, deltaY;
  deltaX = x1 - x2;
  deltaY = y1 - y2;  //Ugh, a simple type bug here had me running bullshit for a week. Yeah, unit testing...
  return sqrt(deltaX*deltaX + deltaY*deltaY);
} 

//Wrapping fprintf(f_log  so we can turn off logging when we want to.
//Wooo! variadic functions!
//Assumes we're outputting to f_log
//Added type in for shits'n'giggles: ERR, gameplay, consiousnessStream, 

int logging(int type, const char* format, ...)
{
  int ret;
  va_list args;

  if(!(g_logging & type)) //mask
    return -1;

  va_start (args, format);
  ret = vfprintf (f_log, format, args); //Yep, well this was specifically written for wrappers.
  va_end (args);

  //Whoa nelly, let's make sure we write our last will and testament before we go down in flames.
  //fflush(f_log);
  //usleep(20);

  return ret;
}

int betterRand(int n)
{
  return rand() / (RAND_MAX / n + 1);
}

float betterFRand(float n)
{
  return (float)rand()/((float)RAND_MAX/n);
}
