//moving all the animation and SDL graphic functions off to the side. They were cluttering up main something fierce.
//Main still makes use of SDL for timers, events, keyboard interface and such.


#include "orbit.h"
#include "globals.h"
#include "gplib.h"

#include <math.h>
#include "SDL2/SDL.h"
#include "SDL2/SDL_ttf.h"


#define RAD 6.28318f
#define CIRCLE_SEGMENTS 30.0f

//Screen attributes
const int FRAMES_PER_SECOND = 20;

//The window,  which is new in SDL2
SDL_Window *g_window;

//I have no idea wtf a render is in this context
SDL_Renderer *g_renderer;


//The surfaces
SDL_Texture *g_screen = NULL;     //Main surface displayed, now a Texture in SDL2
SDL_Surface *g_message = NULL;    //Used for text... which still use surfaces...

//Font
TTF_Font *g_font;
TTF_Font *g_smFont;
SDL_Color textColor = { 254, 255, 255 }; // R, G, B
SDL_Color textRed = { 254, 0, 0 }; // R, G, B




extern struct ships shipArray[MAX_NUM_SHIPS];
extern struct planets  planetArray[NUM_PLANETS];

extern struct path newKingsPath[ROUND_LENGTH_DEFAULT/100];
extern struct path oldKingsPath[ROUND_LENGTH_DEFAULT/100];



int displayInit()
{
  if( SDL_Init( SDL_INIT_EVERYTHING ) == -1 )
  {
    return false;
  }

  g_window = SDL_CreateWindow(
      "GA Orbit",                        // window title
      SDL_WINDOWPOS_UNDEFINED,           // initial x position
      SDL_WINDOWPOS_UNDEFINED,           // initial y position
      SCREEN_WIDTH,                      // width, in pixels
      SCREEN_HEIGHT,                     // height, in pixels
      SDL_WINDOW_OPENGL                  // flags - see below
      );
  if( g_window == NULL )
  {
    return false;
  }

  g_renderer = SDL_CreateRenderer(g_window, -1, 0);

  g_screen = SDL_CreateTexture(g_renderer, SDL_PIXELFORMAT_RGBA8888, SDL_TEXTUREACCESS_TARGET, 640, 480); 


  //Initialize SDL_ttf
  if( TTF_Init() == -1 )
  {
    return false;
  }
  return true;
}

void cleanUp()
{
  SDL_DestroyTexture(g_screen);  
  SDL_DestroyRenderer(g_renderer);  
  SDL_DestroyWindow(g_window);

  //Close the font that was used
  TTF_CloseFont( g_font );

  //Quit SDL_ttf
  TTF_Quit();
  //IMG_Quit();
  SDL_Quit();
}


void apply_surface( int x, int y, int h, int w,  SDL_Texture* source) 
{
}

int load_files()
{
  g_font = TTF_OpenFont( "agencyfb-bold.ttf", 24);
  if( g_font == NULL )
  {
    return false;
  }

  g_smFont = TTF_OpenFont( "agencyfb-bold.ttf", 16);
  if( g_smFont == NULL )
  {
    return false;
  }

  /*
     g_wheel = IMG_Load("text.png");
     if(!g_wheel)
     {
     printf("IMG_Load: %s\n", IMG_GetError());
     return false;
     }
     */
  return true;
}




void drawPixel( SDL_Texture* surface, Uint32 x, Uint32 y, Uint32 color)
{
  if(y >= SCREEN_HEIGHT) { return; }  //hmmmm, sorta ties it to g_screen... but meh
  if(x >= SCREEN_WIDTH) { return; }

  SDL_SetRenderDrawColor( g_renderer, (color & 0x00ff0000) >> 16,
      (color & 0x0000ff00) >> 8,
      (color & 0x000000ff),
      0xff );

  SDL_RenderDrawPoint( g_renderer, x, y);
}

void drawBoxFill( SDL_Texture* surface, Uint32 x1, Uint32 y1, Uint32 x2, Uint32 y2, Uint32 color)
{
  Uint32 x, y;
  if(y1 >= SCREEN_HEIGHT || y2 >= SCREEN_HEIGHT) { return; }  
  if(x1 >= SCREEN_WIDTH || x2 >= SCREEN_WIDTH) { return; }

  //Lame because I don't want to bother learning how to do it right.
  for(x=x1; x<x2; x++)
  {
    for(y=y1; y<y2; y++)
    {
      drawPixel(surface, x,y,color);
    }
  }
}

void drawLine(SDL_Texture* surface, int x1,int y1,int x2,int y2, Uint32 color)
{
  if(y1 >= SCREEN_HEIGHT || y2 >= SCREEN_HEIGHT) { return; }  
  if(x1 >= SCREEN_WIDTH  || x2 >= SCREEN_WIDTH)  { return; }

  SDL_SetRenderDrawColor( g_renderer, (color & 0x00ff0000) >> 16,
      (color & 0x0000ff00) >> 8,
      (color & 0x000000ff),
      0xff );

  SDL_RenderDrawLine(g_renderer, x1, y1, x2, y2);

}


void drawCircle(int x, int y, float size, uint32 color)
{
  float x1, x2, y1, y2; 

  for(float i=0; i<CIRCLE_SEGMENTS; i++)
  {
    x1 = x + size * sin(RAD/CIRCLE_SEGMENTS * i);
    y1 = y + size * cos(RAD/CIRCLE_SEGMENTS * i);
    x2 = x + size * sin(RAD/CIRCLE_SEGMENTS * (i+1));
    y2 = y + size * cos(RAD/CIRCLE_SEGMENTS * (i+1));
    drawLine(g_screen, (int)x1, (int)y1, (int)x2, (int)y2, color);
  }
}


//Wow this is still lame
void fill_circle(SDL_Texture *surface, int cx, int cy, int radius, uint32 color)
{

  double r = (double)radius;
  int x;
  double dx, dy;

  if(radius >  SCREEN_WIDTH)
    return;

  for (dy = 1; dy <= r; dy += 1.0)
  {
    dx = floor(sqrt((2.0 * r * dy) - (dy * dy)));
    x = cx - dx;

    for (; x <= cx + dx; x++)
    {
      drawPixel(g_screen,x, cy+r-dy,color); 
      drawPixel(g_screen,x, cy-r+dy,color); 
    }
  }
}


void drawThrust(struct ships* ship)
{
  if(ship->crashed)
    return;
  SDL_SetRenderDrawColor( g_renderer, 0xff, 0, 0, 0xff); 
  SDL_RenderDrawLine(g_renderer, ship->x, ship->y, ship->x+ship->accelx,ship->y+ship->accely);


  SDL_SetRenderDrawColor( g_renderer, 0xff, 0x00, 0x00, 0xff);
  SDL_RenderDrawLine(g_renderer, ship->x, ship->y, ship->x+ship->accelx,ship->y+ship->accely);
}

void drawShip(struct ships* ship)
{
  int x,y;
  uint32 color;
  //Uh, x marks the spot for now. 
  //But come on, eventually get a wedge, with facing, and some thruster action
  x = ship->x;
  y = ship->y;

  if(ship == g_kingoftheHill)
  {
    color = rand();
    SDL_SetRenderDrawColor( g_renderer, 0x11, 0xbb, 0xbb, 0xff);
    SDL_RenderDrawLine(g_renderer, x-3, y, x,y-5);
    SDL_SetRenderDrawColor( g_renderer, 0x11, 0xbb, 0xbb, 0xff);
    SDL_RenderDrawLine(g_renderer, x, y-5, x+3, y);
  }
  else if( ship == g_newborn)
  {
    color = rand();
    drawLine(g_screen, x-3, y+3, x+3,y+3, color);
  }
  else
  {
    color = ship->color;
  }

  drawLine(g_screen, x-2, y-2, x+2,y+2, color);
  drawLine(g_screen, x-2, y+2, x+2,y-2, color);

  if(ship->fuel > 0)
    drawThrust(ship);
}


void drawText(int x, int y, SDL_Color color, TTF_Font* font, const char* text, ...)
{
  va_list args;
  SDL_Texture* tmpText;
  SDL_Rect rect;

  va_start (args, text);
  vsprintf (g_bugBuff, text, args); //Yep, well this was specifically written for wrappers.
  va_end (args);

  g_message = TTF_RenderText_Solid( font, g_bugBuff, color );
  tmpText = SDL_CreateTextureFromSurface(g_renderer, g_message);
  rect.x = x;
  rect.y = y;
  rect.h = g_message->h;
  rect.w = g_message->w;
  SDL_RenderCopy(g_renderer, tmpText, NULL, &rect);
  SDL_FreeSurface(g_message);
  SDL_DestroyTexture(tmpText);
}

Uint32 animation( )
{
  static int oldTime = 0;
  int i;

  if( SDL_GetTicks() - oldTime < 1000/ FRAMES_PER_SECOND)
  { return 0; }
  oldTime = SDL_GetTicks();

  //Fill the screen with white / erase the last frame
  SDL_SetRenderDrawColor( g_renderer, 0, 0, 0, 255 );
  SDL_RenderClear( g_renderer );

  drawText(0,0, textColor, g_font, "Round: %d  time: %d  ", g_round, g_time);
  drawText(0,30, textColor, g_font, "maxFit: %3.1f -> %3.1f Since: %d ", g_maxFit, GOOD_ENOUGH, g_roundsSinceImprovement);   
  //drawText(350,30, textColor, g_font, "PlayerFit: %3.1f", shipFitness(&shipArray[0]));
  drawText(0,SCREEN_WIDTH-60, textColor, g_font, "1-4 speed, (n)ext round, next (b)etter round");
  drawText(0,SCREEN_WIDTH-30, textColor, g_font, "(v)iew fitness, (s)election");

  /*
  //Debugging the shadow ship
  drawText(0,60, textColor, g_font, "i:%d t:%d x:%f y:%f", currentDNAindex(shipArray[1]),
  shipArray[1].dna[currentDNAindex(shipArray[1])].time,
  shipArray[1].dna[currentDNAindex(shipArray[1])].xThrust,
  shipArray[1].dna[currentDNAindex(shipArray[1])].yThrust);
  */

  if(g_speed == MIN_DISPLAY)    //Early exit, skipping everything but the text
  {
    //SDL_Flip( g_screen );
    SDL_RenderPresent(g_renderer);
    return 1;
  }

  //draw planets
  for(i=0; i<NUM_PLANETS; i++)
  {
    fill_circle(g_screen, planetArray[i].x, planetArray[i].y, planetArray[i].size, planetArray[i].color);
  }

  //Draw ships
  for(i=0; i<g_numShips; i++)
  {
    drawShip(&shipArray[i]);
  }

  //Draw the king's path, from last time.
  for(i=0; i < g_roundLength/PATHPRECISION; i++)
  {
    drawPixel(g_screen, oldKingsPath[i].x, oldKingsPath[i].y, 0x00FFFFFF);
  }

  //Draw the current closest values for each planet for the winning ship
  //Was pretty helpful for finding bugs. 
  if(g_viewFitArray && g_kingoftheHill != NULL)
  {
    for(i=0; i<NUM_PLANETS; i++)
    {
      //fill_circle(g_screen, planetArray[i].x, planetArray[i].y, g_kingoftheHill->minDist[i] , planetArray[i].color | 0x000F0F0F);
      drawCircle(planetArray[i].x, planetArray[i].y, g_kingoftheHill->minDist[i] , planetArray[i].color | 0x000F0F0F);

      SDL_SetRenderDrawColor( g_renderer, 0xff, 0xff, 0xff, 0xff);
      SDL_RenderDrawLine(g_renderer, planetArray[i].x, planetArray[i].y, g_kingoftheHill->x, g_kingoftheHill->y);
    }
    drawText( 0, 60, textColor, g_font,  "%3.1f %3.1f  %3.1f", g_kingoftheHill->minDist[0], g_kingoftheHill->minDist[1], g_kingoftheHill->minDist[2]); 
  }

  SDL_RenderPresent(g_renderer);
  return 1;
}



void drawSelectionBox(int x, int y)
{
  drawLine(g_screen, x-5, y+5, x+140,y+5, rand());    //Quite annoying getting it exact
  drawLine(g_screen, x-5, y-10, x+140,y-10, rand());
  drawLine(g_screen, x-5, y+5, x-5,y-10, rand());
  drawLine(g_screen, x+140, y+5, x+140,y-10, rand());
}


//We are between rounds, mid-selection, and we want to take some time to animate the activities of the program
//  Real self-aware, introspective sort of stuff that's good for a programming demo.   Also crazy handy for debugging.
//Ugh, TODO architecture change so we can have keyboard commands to skip this, exit program. This really shouldn't halt the software
//It's all delayed by FPS limiter, so to hell with optimizing it for speed. 
void selectionDisplay(struct ships* combantant1, 
    struct ships* combantant2, 
    struct ships* dadCompetitor1, 
    struct ships* dadCompetitor2, 
    struct ships* momCompetitor1, 
    struct ships* momCompetitor2, 
    struct ships* dad, 
    struct ships* mom, 
    struct ships* toDie)
{
  int animationStartTime = 0;
  int updateTime = 0;
  int i, x, y;

  return; //DEBUGGING

  if(!g_displaySelection){ return;}

  //Big animation loop
  animationStartTime = SDL_GetTicks();
  updateTime = animationStartTime;
  while(SDL_GetTicks() - animationStartTime < 20000) //20 seconds?  TODO refine me, some sort of #define 
  {
    //get out of this halting thing if we get anything from the keyboard.
    SDL_Event g_event;
    while( SDL_PollEvent( &g_event ) )
    {
      if(g_event.type == SDL_KEYDOWN)
        return;
    }
    if( SDL_GetTicks() - updateTime < 1000/ FRAMES_PER_SECOND)  { continue; }
    updateTime = SDL_GetTicks();

    //Fill the screen with white / erase the last frame
    SDL_SetRenderDrawColor( g_renderer, 20, 20, 20, 255 );
    SDL_RenderClear( g_renderer );

    //Debugging   Display TIME
    drawText(SCREEN_WIDTH-50, 5, textColor, g_font, "%d", SDL_GetTicks() - animationStartTime);


    if(SDL_GetTicks() - animationStartTime < 11000)
    {
      drawText(0,10, textColor, g_font, "ONE ON ONE COMBAT, TO THE DEATH!");
    }
    else
    {
      drawText(0,10, textColor, g_font, "Mating Season");
    }


    //Draw the ships (symbol, name, color, fitness)       ?in order of fitness?
    x = 10;
    y = 50;
    int gapBetweenRows = 15;
    int gapBetweenColumns = 20;
    int textAlignmentOffset = 10;
    for(i=0; i<g_numShips; i++)
    {
      if(i*50 > SDL_GetTicks() - animationStartTime)
      {
        continue;
      }

      int color = shipArray[i].color;

      y += gapBetweenRows;
      drawLine(g_screen, x-2, y-2, x+2,y+2, color);
      drawLine(g_screen, x-2, y+2, x+2,y-2, color);

      drawText( x+gapBetweenColumns,y-textAlignmentOffset, textColor, g_smFont, "%d: %s %3.2f", i, shipArray[i].name, shipArray[i].fitness);

      //Draw lotto selection, comparison, death       
      //Drawing lotto selection #1
      if(SDL_GetTicks() - animationStartTime > 3000  &&
          SDL_GetTicks() - animationStartTime < 11000 &&
          shipArray[i].name == combantant1->name)
      {      
        drawSelectionBox(x,y);
        drawText(300,20, textColor, g_font, "%d: %s %3.2f", i, shipArray[i].name, shipArray[i].fitness);
      }

      //Drawing "VS"
      if(SDL_GetTicks() - animationStartTime > 3500 &&
          SDL_GetTicks() - animationStartTime < 11000 &&
          shipArray[i].name == combantant2->name)
      {      
        drawText(300,50, textColor, g_font, "VS.");
      }

      //Drawing lotto selection #2
      if(SDL_GetTicks() - animationStartTime > 4000 &&
          SDL_GetTicks() - animationStartTime < 11000 &&
          shipArray[i].name == combantant2->name)
      {      
        drawSelectionBox(x,y);
        drawText(300,80, textColor, g_font, "%d: %s %3.2f", i, shipArray[i].name, shipArray[i].fitness);
      }

      //Drawing DEATH
      if(SDL_GetTicks() - animationStartTime > 7000 &&
          SDL_GetTicks() - animationStartTime < 11000 &&
          shipArray[i].name == toDie->name)
      {      
        SDL_SetRenderDrawColor( g_renderer, 0xFF, 0x00, 0x00, 0xff);
        SDL_RenderDrawLine(g_renderer, x+5, y+rand()%15-5, x+135,y+rand()%15-5);
        SDL_SetRenderDrawColor( g_renderer, 0xFF, 0x00, 0x00, 0xff);
        SDL_RenderDrawLine(g_renderer, x+5, y+rand()%15-5, x+135,y+rand()%15-5);
        SDL_SetRenderDrawColor( g_renderer, 0xFF, 0x00, 0x00, 0xff);
        SDL_RenderDrawLine(g_renderer, x+5, y+rand()%15-5, x+135,y+rand()%15-5);
        drawText(x+110,y-textAlignmentOffset, textColor, g_font, "DEAD!");
        if(toDie == combantant2) 
        {
          SDL_SetRenderDrawColor( g_renderer, 0xFF, 0x00, 0x00, 0xff);
          SDL_RenderDrawLine(g_renderer, 300, 95+rand()%30-15, 500,95+rand()%30-15);
          SDL_SetRenderDrawColor( g_renderer, 0xFF, 0x00, 0x00, 0xff);
          SDL_RenderDrawLine(g_renderer, 300, 95+rand()%30-15, 500,95+rand()%30-15);
          SDL_SetRenderDrawColor( g_renderer, 0xFF, 0x00, 0x00, 0xff);
          SDL_RenderDrawLine(g_renderer, 300, 95+rand()%30-15, 500,95+rand()%30-15);
        }
        else
        {
          SDL_SetRenderDrawColor( g_renderer, 0xFF, 0x00, 0x00, 0xff);
          SDL_RenderDrawLine(g_renderer, 300, 35+rand()%30-15, 500,35+rand()%30-15);
          SDL_SetRenderDrawColor( g_renderer, 0xFF, 0x00, 0x00, 0xff);
          SDL_RenderDrawLine(g_renderer, 300, 35+rand()%30-15, 500,35+rand()%30-15);
          SDL_SetRenderDrawColor( g_renderer, 0xFF, 0x00, 0x00, 0xff);
          SDL_RenderDrawLine(g_renderer, 300, 35+rand()%30-15, 500,35+rand()%30-15);
        }
      }

      //clearing dead guy,  lame to place it here, but it makes it easy to read
      if(SDL_GetTicks() - animationStartTime > 11000 &&
          SDL_GetTicks() - animationStartTime < 20000 &&
          shipArray[i].name == toDie->name)
      { 
        drawBoxFill(g_screen,x-5,y-10, x+150, y+90, 0x00000000);
      }

      //TODO play with the timing and placement,   slow it down for debugging purposes.
      //Show love lotto, comparison, placing it into mother position
      if(SDL_GetTicks() - animationStartTime > 13000 &&
          SDL_GetTicks() - animationStartTime < 16000 &&
          shipArray[i].name == momCompetitor1->name)
      {      
        drawSelectionBox(x,y);  
        drawText(300, 110, textColor, g_font, "Mother Competition:");
        drawText(300, 140, textColor, g_font, "%d: %s %3.2f", i, shipArray[i].name, shipArray[i].fitness);
      }

      if(SDL_GetTicks() - animationStartTime > 13500 &&
          SDL_GetTicks() - animationStartTime < 16000 &&
          shipArray[i].name == momCompetitor2->name)
      { 
        drawText(300, 170, textColor, g_font, "VS.");      
      }

      if(SDL_GetTicks() - animationStartTime > 14000 &&
          SDL_GetTicks() - animationStartTime < 16000 &&
          shipArray[i].name == momCompetitor2->name)
      {      
        drawSelectionBox(x,y);  
        drawText(300, 200, textColor, g_font, "%d: %s %3.2f", i, shipArray[i].name, shipArray[i].fitness);
      }
      // Winning Mother
      if(SDL_GetTicks() - animationStartTime > 16000 &&
          SDL_GetTicks() - animationStartTime < 20000 &&
          shipArray[i].name == mom->name)
      { 
        drawText(300, 110, textColor, g_font, "Mother:");
        drawText(300, 140, textColor, g_font, "%d: %s %3.2f", i, shipArray[i].name, shipArray[i].fitness);
        //TODO draw a heart to something
      }




      //Show love lotto, comparison, placing it onto father position
      if(SDL_GetTicks() - animationStartTime > 16000 &&
          SDL_GetTicks() - animationStartTime < 18000 &&
          shipArray[i].name == dadCompetitor1->name)
      {      
        drawSelectionBox(x,y);  
        drawText(300, 200, textColor, g_font, "Father Competition:");
        drawText(300, 230, textColor, g_font, "%d: %s %3.2f", i, shipArray[i].name, shipArray[i].fitness);
      }

      if(SDL_GetTicks() - animationStartTime > 16500 &&
          SDL_GetTicks() - animationStartTime < 18000 &&
          shipArray[i].name == dadCompetitor1->name)
      {      
        drawText(300, 260, textColor, g_font, "VS.");
      }

      if(SDL_GetTicks() - animationStartTime > 17000 &&
          SDL_GetTicks() - animationStartTime < 18000 &&
          shipArray[i].name == dadCompetitor2->name)
      {      
        drawSelectionBox(x,y);  
        drawText(300, 290, textColor, g_font, "%d: %s %3.2f", i, shipArray[i].name, shipArray[i].fitness);
      }
      // Winning Father
      if(SDL_GetTicks() - animationStartTime > 18000 &&
          SDL_GetTicks() - animationStartTime < 20000 &&
          shipArray[i].name == dad->name)
      { 
        drawText(300, 200, textColor, g_font, "Father:");
        drawText(300, 230, textColor, g_font, "%d: %s %3.2f", i, shipArray[i].name, shipArray[i].fitness);
      }

      //Keep that big list from going off screen
      if(y > SCREEN_HEIGHT - 20)
      {
        x += 150;
        y = 50;
      } 
    }

    SDL_RenderPresent(g_renderer);
  }

}


void breedDisplay(struct ships* dad, 
    struct ships* mom, 
    struct ships* kid,
    int blendPoint)
{
  int animationStartTime = 0;
  int updateTime = 0;

  int i;

  if(!g_displaySelection){ return;}

  //Big animation loop
  animationStartTime = SDL_GetTicks();
  updateTime = animationStartTime;
  while(SDL_GetTicks() - animationStartTime < 15000) //20 seconds?  TODO refine me, some sort of #define 
  {
    //get out of this halting thing if we get anything from the keyboard.
    SDL_Event g_event;
    while( SDL_PollEvent( &g_event ) )
    {
      if(g_event.type == SDL_KEYDOWN)
        return;
    }
    if( SDL_GetTicks() - updateTime < 1000/ FRAMES_PER_SECOND)  { continue; }
    updateTime = SDL_GetTicks();

    //Fill the screen with white / erase the last frame
    SDL_SetRenderDrawColor( g_renderer, 20, 20, 20, 255 );
    SDL_RenderClear( g_renderer );

    //Display header and whatnot
    drawText(0, 10, textColor, g_font, "Breeding");

    drawText(30, 40, textColor, g_font, "Father:");
    drawLine(g_screen, 26, 70, 30,74, dad->color);
    drawLine(g_screen, 26, 74, 30,70, dad->color);
    drawText(30, 70, textColor, g_font, "%s %3.2f", dad->name , dad->finalFitness);

    drawText(230, 40, textColor, g_font, "Mother:");
    drawLine(g_screen, 226, 70, 230,74, mom->color);
    drawLine(g_screen, 226, 74, 230,70, mom->color);
    drawText(230, 70, textColor, g_font, "%s %3.2f", mom->name , mom->finalFitness);

    //Show DNA 
    //TODO halting issue that freezes program but timesout to continue the next round. Only happens in later bots, so.... probably something with displaying weird DNA or something with mutations.
    for(i=0; i<DNA_SIZE; i++)
    {
      Uint32 color;
      //Time is a white bar, X thrust extending to the left, Ythrust to the right, color based on positive or negative
      int time = dad->dna[i].time/10;
      SDL_SetRenderDrawColor( g_renderer, 0xFF, 0xFF, 0xFF, 0xff);
      SDL_RenderDrawLine(g_renderer, 40-0.5*time, 101+i, 40+0.5*time,101+i);
      color = (dad->dna[i].xThrust < 0) ? 0x00FF0000 : 0x0000FF00;   //Ternary ops, bitches!
      drawLine(g_screen, 80, 100+i, 80-abs(dad->dna[i].xThrust),100+i, color);

      color = (dad->dna[i].yThrust < 0) ? 0x00FF0000 : 0x0000FF00;   //Ternary ops, bitches!
      drawLine(g_screen, 80, 100+i, 80+abs(dad->dna[i].yThrust),100+i, color);

      //Same thing for mother
      time = mom->dna[i].time/10;
      SDL_SetRenderDrawColor( g_renderer, 0xFF, 0xFF, 0xFF, 0xff);
      SDL_RenderDrawLine(g_renderer, 240-0.5*time, 101+i, 240+0.5*time,101+i);
      color = (mom->dna[i].xThrust < 0) ? 0x00FF0000 : 0x0000FF00;   //Ternary ops, bitches!
      drawLine(g_screen, 280, 100+i, 280-abs(mom->dna[i].xThrust),100+i, color);

      color = (mom->dna[i].yThrust < 0) ? 0x00FF0000 : 0x0000FF00;   //Ternary ops, bitches!
      drawLine(g_screen, 280, 100+i, 280+abs(mom->dna[i].yThrust),100+i, color);
    }


    //Show new guy with name and new color
    if(SDL_GetTicks() - animationStartTime  > 2000)
    {
      drawText(430, 40, textColor, g_font, "Kid:");
      drawLine(g_screen, 426, 70, 430,74, kid->color);
      drawLine(g_screen, 426, 74, 430,70, kid->color);
      drawText(430, 70, textColor, g_font, "%s", kid->name);  
    }


    //Show DNA mixing
    for(i=0; i<DNA_SIZE; i++)
    {
      Uint32 color;

      if(SDL_GetTicks() - animationStartTime  < 2000+i*20) continue;


      //Time is a white bar, X thrust extending to the left, Ythrust to the right, color based on positive or negative
      int time = kid->dna[i].time/10;
      SDL_SetRenderDrawColor( g_renderer, 0xFF, 0xFF, 0xFF, 0xff);
      SDL_RenderDrawLine(g_renderer, 440-0.5*time, 101+i, 440+0.5*time,101+i);
      color = (kid->dna[i].xThrust < 0) ? 0x00FF0000 : 0x0000FF00;   //Ternary ops, bitches!
      drawLine(g_screen, 480, 100+i, 480-abs(kid->dna[i].xThrust),100+i, color);

      color = (kid->dna[i].yThrust < 0) ? 0x00FF0000 : 0x0000FF00;   //Ternary ops, bitches!
      drawLine(g_screen, 480, 100+i, 480+abs(kid->dna[i].yThrust),100+i, color);

      //draw line by father, switching over to mother
      int fatherExtent = (i<blendPoint)? i : blendPoint;
      drawLine(g_screen, 60, 100, 60, 100+fatherExtent, kid->color);
      if(i > blendPoint)
        drawLine(g_screen, 260, 100+blendPoint, 260, 100+i, kid->color);
    }




    //Show mutation    //TODO finish cleaning this up!
    for(i=0; i<20; i++)
    {
      Uint32 color = 0x00FF0000;
      char name[20];

      if(kid->mutations[i].mutationLocation == -1) continue;

      if(SDL_GetTicks() - animationStartTime  < 7000+i*500) continue;

      int yPosition1 = 100+ kid->mutations[i].mutationLocation;
      int yPosition2 = yPosition1 + kid->mutations[i].mutationSize;
      drawLine(g_screen, 400,yPosition1, 430, yPosition1, color);
      drawLine(g_screen, 400,yPosition1, 400, yPosition2, color);
      drawLine(g_screen, 400,yPosition2, 430, yPosition2, color);


      mutationName(name, kid->mutations[i].mutationType);
      drawText(50, 400+i*15, textColor, g_font, "Mutation: %s", name);  

    }



    SDL_RenderPresent(g_renderer);
  }
}
