
#include <math.h>
#include "SDL2/SDL.h"
#include "SDL2/SDL_ttf.h"



int displayInit();
void cleanUp();
void apply_surface( int x, int y, SDL_Surface* source, SDL_Surface* destination );
int load_files();
void drawPixel(SDL_Surface* surface, Uint32 x, Uint32 y, Uint32 color);
void drawLine(SDL_Surface* surface, int x1,int y1,int x2,int y2, Uint32 color);
void drawCircle(int x, int y, float size, uint32 color);
void fill_circle(SDL_Surface *surface, int cx, int cy, int radius, uint32 color);
void drawThrust(struct ships* ship);
void drawShip(struct ships* ship);
Uint32 animation( );

void selectionDisplay(struct ships* combantant1, struct ships* combantant2, struct ships* dadCompetitor1, struct ships* dadCompetitor2, struct ships* momCompetitor1, struct ships* momCompetitor2, struct ships* dad, struct ships* mom, struct ships* toDie);
void breedDisplay(struct ships* dad, struct ships* mom, struct ships* toDie, int blendPoint);

