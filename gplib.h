#ifndef __GPLIB__
#define __GPLIB__
/* 	
*/

#include <stdio.h>
#include "globals.h"
#include "orbit.h"

extern FILE* f_log;
extern FILE* f_mutationchart;
extern FILE* f_fitchart;

void breedShips(struct ships* dad, struct ships* mom, struct ships* baby);
void mutateShip(struct ships* ship);
void mutateName(char* name);
void updateFitness(struct ships pool[], struct planets targets[]);
void printDNA(struct ships* ship);
void fprintDNA(FILE* f, struct ships* ship);
void chartLog();
void copyDNA(struct DNA from[], struct DNA to[]);
int currentDNAindex(struct ships ship);
void mutationName(char *name, int mutationType);


void selection(struct ships pool[] );
float shipFitness(struct ships* ship);
#endif
